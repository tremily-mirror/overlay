# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

DESCRIPTION="Virtual for WTK's blog build and scripts"
HOMEPAGE="http://blog.tremily.us/"
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="app-admin/conky[X,mpd]
	app-text/ghostscript-gpl
	app-text/itex2mml
	app-text/pdftk
	dev-python/geopy
	dev-python/pyproj
	dev-python/python-ldap
	dev-python/suds
	media-fonts/freefonts
	media-gfx/exif
	media-gfx/fontforge[python]
	media-gfx/imagemagick
	media-libs/faad2
	media-libs/flac
	media-libs/mutagen
	media-sound/abcmidi
	media-sound/lame
	media-sound/lilypond
	media-sound/mpg123
	media-sound/timidity++
	media-sound/vorbis-tools
	media-video/ffmpeg
	media-video/ffmpeg2theora
	perl-gcpan/MathML-Entities
	www-apps/ikiwiki[extras]
	www-apps/scgi
	"
