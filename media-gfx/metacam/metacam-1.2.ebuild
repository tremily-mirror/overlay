# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"

inherit eutils

DESCRIPTION="Camera Image Meta-Information Reader"
HOMEPAGE="http://www.cheeseplant.org/~daniel/pages/metacam.html"
SRC_URI="http://www.cheeseplant.org/metacam/downloads/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86"
IUSE=""

src_prepare() {
	epatch "${FILESDIR}/${P}-include.patch"
}

src_compile() {
	emake || die
}

src_install() {
	into /usr
	dobin metacam

	dodoc BUGS LICENSE.txt README* THANKS layout.txt
}
