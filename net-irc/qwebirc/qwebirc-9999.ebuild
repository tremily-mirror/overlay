# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
PYTHON_COMPAT=( python{2_6,2_7} )

inherit eutils python-single-r1

DESCRIPTION="A multi-user IRC client, used by QuakeNet, Freenode, etc."
HOMEPAGE="http://qwebirc.org"

if [[ "${PV}" == "9999" ]]; then
	inherit mercurial
	EHG_REPO_URI=http://hg.qwebirc.org/qwebirc/
	SRC_URI=""
else
	SRC_URI=""  # upstream doesn't cut releases, see http://qwebirc.org/download
fi

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-lang/python-2.5
	dev-python/pyopenssl
	dev-python/simplejson
	>=dev-python/twisted-mail-8.2
	>=dev-python/twisted-names-8.2
	>=dev-python/twisted-runner-8.2
	>=dev-python/twisted-web-8.2
	>=dev-python/twisted-words-8.2
	net-zope/zope-interface
	>=virtual/jre-1.6
"
DEPEND="${RDEPEND}"

pkg_setup() {
	ebegin "Creating ${PN} user and group"
	enewgroup "${PN}"
	enewuser "${PN}" -1 -1 -1 "${PN}"
	eend $?
}

src_compile() {
	cp config.py{.example,}
	"${EPYTHON}" ./compile.py
	python_fix_shebang run.py
}

src_install() {
	insinto /var/www/qwebirc/
	doins -r bin css dummyauthgate esimplejson js qwebirc static twisted util
	doins -r .checked .compiled
	doins README AUTHORS LICENSE *.py qwebirc.pdn qwebirc.png
	chown -R "${PN}:${PN}" "${ED}/var/www/qwebirc/"

	# create the logfile as qwebirc does not have the permission to
	# create it itself
	dodir /var/log
	touch "${ED}/var/log/qwebirc.log"
	fowners qwebirc:qwebirc /var/log/qwebirc.log
	fperms 640 /var/log/qwebirc.log

	newinitd "${FILESDIR}/qwebirc.initd" qwebirc
	newconfd "${FILESDIR}/qwebirc.confd" qwebirc
}
