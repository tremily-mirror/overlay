# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit eutils

DESCRIPTION="X11 connection monitor and proxy"
HOMEPAGE="http://cern.ch/mxconns/ http://mxconns.web.cern.ch/mxconns/"
SRC_URI="http://mxconns.web.cern.ch/mxconns/${P}.tgz"

LICENSE="CERN"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="x11-libs/libXau
	x11-libs/openmotif"
RDEPEND="$DEPEND"

src_configure() {
	./configure
}

src_compile() {
	emake "CFLAGS=${CFLAGS}" "LDFLAGS=${LDFLAGS}"
  cp mxconns.man mxconns.1
}

src_install() {
	dobin mxconns
  doman mxconns.1
}
