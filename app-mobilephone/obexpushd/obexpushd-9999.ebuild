# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit cmake-utils

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_REPO_URI="git://gitorious.org/obexpushd/mainline.git"
	EGIT_PROJECT="${PN}.git"
	SRC_URI=""
else
	inherit versionator
	MY_MAJOR_MINOR=$(version_format_string '$1.$2')
	SRC_URI="http://www.hendrik-sattler.de/downloads/obexpushd/${MY_MAJOR_MINOR}/obexpushd-${PV}-source.tar.gz"
	S="${WORKDIR}/${P}-source"
fi

DESCRIPTION="Simple OBEX push daemon based on OpenOBEX"
HOMEPAGE="http://www.gitorious.org/obexpushd"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="doc tcpd threads -xobex xattr"

RDEPEND=">=dev-libs/openobex-1.5[bluetooth]
	net-wireless/bluez
	sys-kernel/linux-headers
	tcpd? ( sys-apps/tcp-wrappers )
	xattr? ( sys-apps/attr )"

DEPEND="dev-util/pkgconfig
	doc? (
		|| ( dev-libs/libxslt dev-java/saxon dev-libs/xalan )
		app-text/docbook-xsl-stylesheets
	)
	${RDEPEND}"

src_configure() {
	mycmakeargs=(
		-DDOCUMENTATION_INSTALL_DIR="/usr/share/doc/${PF}"
		$(cmake-utils_use doc BUILD_HELP_HTML)
		$(cmake-utils_use doc BUILD_HELP_MAN)
		$(cmake-utils_use_use threads THREADS)
		$(cmake-utils_use_enable tcpd TCPWRAP)
		$(cmake-utils_use xobex BUILD_X_OBEX_SHARED_LIBS)
		$(cmake-utils_use_use xattr XATTR)
	)
	cmake-utils_src_configure
}

src_install() {
	cmake-utils_src_install
	if ! use doc; then
		rm -rf "${D}/usr/share"  # nothing there except /doc/${PF}
	fi
}
