# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit eutils git-2

EGIT_BRANCH="master"
EGIT_REPO_URI="git://comedi.org/git/comedi/comedi.git"
SRC_URI=""

DESCRIPTION="Header files for Control and Measurement Device Interface kernel modules"
HOMEPAGE="http://www.comedi.org/"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="+udev"

DEPEND=""
RDEPEND=""

RESTRICT="binchecks strip"

pkg_setup() {
	use udev && enewgroup comedi
	#groupdel in pkg_postrm()?
}

src_configure() { :; }

src_compile() { :; }

src_install() {
	cd "${S}/include/linux"
	insinto /usr/include/
	doins comedi.h || die "include install failed"
	if use udev; then
		insinto /etc/udev/rules.d
		doins "${FILESDIR}/52-comedi.rules" || die "udev rule install failed"
	fi
}

pkg_postinst() {
	if use udev; then
		elog "To be able to use Comedi devices, you need to be a"
		elog "member of the group 'comedi' which has just been added"
		elog "to your system. You can add your user to the group by"
		elog "running the following command as root:"
		elog
		elog "	usermod -a -G comedi youruser"
		elog
		elog "Please be aware that you need to either re-login or run"
		elog
		elog "	newgrp - comedi"
		elog
		elog "for the group membership to take effect."
	fi
}
