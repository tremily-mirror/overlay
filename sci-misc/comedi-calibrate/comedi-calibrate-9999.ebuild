# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit autotools

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_BRANCH="master"
	EGIT_REPO_URI="git://comedi.org/git/comedi/${PN/-/_}.git"
	SRC_URI=""
else
	SRC_URI="http://www.comedi.org/download/${PN/-/_}-${PV}.tar.gz"
fi

DESCRIPTION="Comedi calibration utilities."
HOMEPAGE="http://www.comedi.org"

IUSE="doc"
KEYWORDS="~x86 ~amd64"
LICENSE="LGPL-2.1"
SLOT="0"

RDEPEND=">=sci-libs/comedilib-${PV}
	sci-libs/gsl
	dev-libs/boost"
DEPEND="${RDEPEND}
	sci-libs/gsl
	sys-devel/flex
	sys-devel/bison"

src_prepare()
{
	if [ "${PV}" == "9999" ] ; then
		eautoreconf
	fi
}

src_configure()
{
	# by default, Gentoo uses `"${EPREFIX}"/var/lib` for
	# localstatedir, but the GNU folks recommend $(prefix)/var.
	#   http://www.gnu.org/prep/standards/html_node/Directory-Variables.html
	# Comedi expects to be configured with the GNU form, otherwise we
	# get /var/lib/lib/comedi.
	econf --localstatedir="${EPREFIX}"/var
}

src_install()
{
	emake DESTDIR="${D}" install
	dodoc README AUTHORS ChangeLog
	# make the calibration directory available to the comedi group set
	# up by sci-misc/comedi-headers
	chgrp comedi "${D}/var/lib/comedi/calibrations"
	chmod 775 "${D}/var/lib/comedi/calibrations"
}
