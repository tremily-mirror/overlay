# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

MY_PN="tornado_pyvows"
MY_P="${MY_PN}-${PV}"

if [[ "${PV}" == "9999" ]]; then
	inherit git-2
	EGIT_REPO_URI="git://github.com/rafaelcaricio/${MY_PN}.git"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${MY_P:0:1}/${MY_PN}/${MY_P}.tar.gz -> ${P}.tar.gz"
fi

DESCRIPTION="Extensions to test Tornado apps under pyvows"
HOMEPAGE="https://github.com/rafaelcaricio/tornado_pyvows https://pypi.python.org/pypi/tornado_pyvows"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND="
	dev-python/pycurl[${PYTHON_USEDEP}]
	dev-python/pyvows[${PYTHON_USEDEP}]
	!=dev-python/pyvows-2.0.4[${PYTHON_USEDEP}]
	!=dev-python/pyvows-2.0.5[${PYTHON_USEDEP}]
	dev-python/urllib3[${PYTHON_USEDEP}]
	www-servers/tornado[${PYTHON_USEDEP}]
	"
DEPEND="
	test? (
		${RDEPEND}
		dev-python/mock[${PYTHON_USEDEP}]
	)"

S="${WORKDIR}/${MY_P}"

python_test() {
	"${EPYTHON}" -c 'import pyvows.cli; pyvows.cli.main()' vows/ || die "tests failed with ${EPYTHON}"
}
