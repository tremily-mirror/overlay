# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="6"
PYTHON_COMPAT=( python2_7 )

inherit eutils distutils-r1

if [[ ${PV} == "9999" ]] ; then
	inherit mercurial
	EHG_REPO_URI="https://bitbucket.org/trevor/${PN}"
	SRC_URI=""
else
	SRC_URI=""
fi

DESCRIPTION="Track what you spend time on via this simple command line application"
HOMEPAGE="https://bitbucket.org/trevor/${PN}/overview"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND="dev-lang/python[sqlite]"
DEPEND=""

src_unpack() {
	if [[ ${PV} == "9999" ]] ; then
		mercurial_src_unpack
	else
		unpack ${A}
	fi
	cd "${S}"
}

src_install() {
	distutils-r1_src_install
	dodoc README
}
