# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=6
PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

MY_PN="pyVows"
MY_P="${MY_PN}-${PV}"

if [[ "${PV}" == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="git://github.com/heynemann/${PN}.git"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${MY_P:0:1}/${MY_PN}/${MY_P}.tar.gz -> ${P}.tar.gz"
fi

DESCRIPTION="A test engine based on Vows.js"
HOMEPAGE="http://pyvows.org/ https://pypi.python.org/pypi/pyVows"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND="
	>=dev-python/gevent-0.13.6[${PYTHON_USEDEP}]
	>=dev-python/preggy-0.11.1[${PYTHON_USEDEP}]
	"
DEPEND="
	test? (
		${RDEPEND}
		dev-python/colorama[${PYTHON_USEDEP}]
		dev-python/coverage[${PYTHON_USEDEP}]
	)"

S="${WORKDIR}/${MY_P}"

python_prepare_all() {
	sed -i "s/packages=find_packages()/packages=find_packages(exclude=['tests', 'tests.*'])/" setup.py || die
	distutils-r1_python_prepare_all
}

python_test() {
	"${EPYTHON}" -c 'import coverage; coverage.main()' run --source=pyvows ./pyvows/__main__.py ||
		die "tests failed with ${EPYTHON}"
}
