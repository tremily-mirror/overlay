# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_COMPAT=( python3_3 )

inherit distutils-r1

if [[ "${PV}" == "9999" ]]; then
	inherit mercurial
	EHG_REPO_URI="https://code.google.com/p/tulip/"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"
fi

DESCRIPTION="Reference implementation of PEP 3156 for Python 3.3"
HOMEPAGE="http://www.python.org/dev/peps/pep-3156/ https://pypi.python.org/pypi/asyncio"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND=""
DEPEND=""

python_test() {
	"${EPYTHON}" runtests.py
}
