# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=6
PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

if [[ "${PV}" == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="git://github.com/globocom/${PN}.git"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"
fi

DESCRIPTION="Abstracts configuration file loading"
HOMEPAGE="https://github.com/globocom/derpconf https://pypi.python.org/pypi/derpconf"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND=""
DEPEND="
	dev-python/six[${PYTHON_USEDEP}]
	test? (
		dev-python/colorama[${PYTHON_USEDEP}]
		dev-python/coverage[${PYTHON_USEDEP}]
		dev-python/gevent[${PYTHON_USEDEP}]
		dev-python/pyvows[${PYTHON_USEDEP}]
		dev-python/tox[${PYTHON_USEDEP}]
	)"

python_test() {
	make test
}
