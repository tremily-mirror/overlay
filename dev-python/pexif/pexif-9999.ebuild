# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=6
PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

if [[ "${PV}" == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="git://github.com/bennoleslie/pexif.git"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"
fi

DESCRIPTION="Edit JPEG EXIF data"
HOMEPAGE="https://github.com/bennoleslie/pexif https://pypi.python.org/pypi/pexif"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND=""
DEPEND=""

python_test() {
	PYTHONPATH=. "${EPYTHON}" ./test/test.py
}
