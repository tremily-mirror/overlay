# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

PYTHON_DEPEND="2:2.6"
SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="3.*"

inherit eutils distutils

MY_PN="splittable_kwargs" #${PN/-/_}"

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_REPO_URI="git://tremily.us/${MY_PN}.git"
	SRC_URI=""
else
	SRC_URI="http://git.tremily.us/?p=${MY_PN}.git;a=snapshot;h=v${PV};sf=tgz"
fi

DESCRIPTION="Split **kwargs arguments among several functions."
HOMEPAGE="http://blog.tremily.us/posts/${MY_PN}/"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="test"

src_unpack() {
	if [[ ${PV} == "9999" ]] ; then
		git-2_src_unpack
	else
		unpack ${A}
	fi
	cd "${S}"
}

src_install() {
	distutils_src_install
	dodoc README
}
