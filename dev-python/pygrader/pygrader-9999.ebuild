# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
PYTHON_DEPEND="3:3.2"
SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="2.* 3.[01]"

inherit eutils distutils

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_BRANCH="master"
	EGIT_REPO_URI="git://tremily.us/${PN}.git"
	SRC_URI=""
else
	SRC_URI="http://git.tremily.us/?p=${PN}.git;a=snapshot;h=v${PV};sf=tgz"
fi

DESCRIPTION="Python tools for dealing with Pretty Good Privacy (PGP) and email."
HOMEPAGE="http://blog.tremily.us/posts/${PN}"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc test"

RDEPEND="dev-python/jinja
	dev-python/numpy
	dev-python/pgp-mime"
DEPEND="${RDEPEND}
	test? ( dev-python/nose )"

src_unpack() {
	if [[ ${PV} == "9999" ]] ; then
		git-2_src_unpack
	else
		unpack ${A}
	fi
	cd "${S}"
}

src_install() {
	distutils_src_install
	dodoc README
	if use doc; then
		dodoc -r test
	fi
}
