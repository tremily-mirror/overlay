# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
PYTHON_COMPAT=( python{2_6,2_7,3_2,3_3,3_4} )

inherit distutils-r1

DESCRIPTION="A built-package format for Python"
HOMEPAGE="https://github.com/jiffyclub/ipythonblocks/"
if [[ "${PV}" == "9999" ]]; then
	inherit git-2
	EGIT_BRANCH="master"
	EGIT_REPO_URI="git://github.com/jiffyclub/${PN}.git"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
fi

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="examples test"

RDEPEND="${PYTHON_DEPS}
	dev-python/ipython
	test? ( >=dev-python/pytest-2.3 )
	examples? ( dev-python/imaging )
	"
DEPEND="${PYTHON_DEPS}"

src_install() {
	distutils-r1_src_install
	dodoc README.rst
	if use examples; then
		dodoc -r demos
	fi
}
