# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
PYTHON_COMPAT=( python{2_6,2_7,3_2,3_3,3_4} )

inherit distutils-r1

DESCRIPTION="Parse strings using a specification based on the Python format() syntax."
HOMEPAGE="https://github.com/r1chardj0n3s/parse http://pypi.python.org/pypi/parse"
if [[ "${PV}" == "9999" ]]; then
	inherit git-2
	EGIT_REPO_URI="git://github.com/r1chardj0n3s/parse.git"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
fi

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"
DOCS="README.rst"

RDEPEND="${PYTHON_DEPS}"
DEPEND="${RDEPEND}"

python_test() {
	"${PYTHON}" test_parse.py || die "Tests failed with ${EPYTHON}"
}
