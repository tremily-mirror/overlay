# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_COMPAT=( python{2_7,3_3,3_4} pypy )

inherit distutils-r1

if [[ "${PV}" == "9999" ]]; then
	inherit git-2
	EGIT_REPO_URI="git://github.com/Fluxx/${PN}.git"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"
fi

DESCRIPTION="Helpers for better testing"
HOMEPAGE="https://github.com/Fluxx/exam https://pypi.python.org/pypi/exam"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND="
	dev-python/mock[${PYTHON_USEDEP}]
	"
DEPEND="
	test? (
		${RDEPEND}
		dev-python/nose[${PYTHON_USEDEP}]
	)"

python_prepare_all() {
	sed -i "s/packages=find_packages()/packages=find_packages(exclude=['tests', 'tests.*'])/" setup.py || die
	distutils-r1_python_prepare_all
}

python_test() {
	esetup.py test
}
