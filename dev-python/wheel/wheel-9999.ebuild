# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
PYTHON_COMPAT=( python{2_6,2_7,3_2,3_3,3_4} )

inherit distutils-r1

DESCRIPTION="A built-package format for Python"
HOMEPAGE="https://bitbucket.org/dholth/wheel/ http://pypi.python.org/pypi/wheel"
if [[ "${PV}" == "9999" ]]; then
	inherit mercurial
	EHG_REPO_URI="https://bitbucket.org/dholth/wheel"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
fi

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

RDEPEND="${PYTHON_DEPS}"
DEPEND="${RDEPEND}"

src_install() {
	distutils-r1_src_install
	dodoc README.txt
	if use doc; then
		dodoc -r docs
	fi
}
