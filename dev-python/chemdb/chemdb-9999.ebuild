# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="3.*"

inherit eutils distutils

if [[ "${PV}" == "9999" ]] ; then
	inherit git-2
	EGIT_REPO_URI="git://tremily.us/${PN}.git"
	SRC_URI=""
else
	SRC_URI="http://git.tremily.us/?p=${PN}.git;a=snapshot;h=${PV};sf=tgz"
fi

DESCRIPTION="Chemical database with inventory and door-warning generation."
HOMEPAGE="http://blog.tremily.us/posts/ChemDB/"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="test"

DEPEND="
	dev-python/django
	dev-python/django-grappelli"
RDEPEND="${DEPEND}"

src_unpack() {
	if [[ ${PV} == "9999" ]] ; then
		git-2_src_unpack
	else
		unpack ${A}
	fi
	cd "${S}"
}
