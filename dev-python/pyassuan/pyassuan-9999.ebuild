# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="6"
PYTHON_COMPAT=( python{3_3,3_4,3_5,3_6} )

inherit eutils distutils-r1

if [[ ${PV} == "9999" ]] ; then
	inherit git-r3
	EGIT_REPO_URI="git://tremily.us/${PN}.git"
	SRC_URI=""
else
	SRC_URI="http://git.tremily.us/?p=${PN}.git;a=snapshot;h=v${PV};sf=tgz"
fi

DESCRIPTION="Python module and tools for communicating in the Assuan protocol."
HOMEPAGE="http://blog.tremily.us/posts/${PN}"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc test"

RDEPEND=""
DEPEND="${RDEPEND}"

src_unpack() {
	if [[ ${PV} == "9999" ]] ; then
		git-r3_src_unpack
	else
		unpack ${A}
	fi
	cd "${S}"
}

src_install() {
	distutils-r1_src_install
	dodoc README
}
