# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
PYTHON_DEPEND="2:2.6 3:3.2"
SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="3.[01]"

inherit eutils distutils

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_REPO_URI="git://github.com/agrover/${PN}.git"
	EGIT_MASTER="master"
	SRC_URI=""
else
	inherit vcs-snapshot
	SRC_URI="https://github.com/agrover/${PN}/tarball/v${PV} -> ${P}.tar.gz"
fi

DESCRIPTION="Pythonic wrappers around the libkmod kernel module management."
HOMEPAGE="https://github.com/agrover/${PN}"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86 ~amd64"

RDEPEND="sys-apps/kmod"
DEPEND="$RDEPEND
	>=dev-python/cython-0.16"

src_install() {
	distutils_src_install
	dodoc README
}
