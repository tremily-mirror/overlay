# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=6
PYTHON_COMPAT=( python{2_7,3_3,3_4} pypy )

inherit distutils-r1

if [[ "${PV}" == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="git://github.com/heynemann/${PN}.git"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"
fi

DESCRIPTION="An assertion library for Python"
HOMEPAGE="http://heynemann.github.io/preggy https://pypi.python.org/pypi/preggy"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND="
	dev-python/six[${PYTHON_USEDEP}]
	dev-python/unidecode[${PYTHON_USEDEP}]
	"
DEPEND="
	test? (
		${RDEPEND}
		dev-python/nose[${PYTHON_USEDEP}]
		dev-python/coverage[${PYTHON_USEDEP}]
	)"

python_prepare_all() {
	sed -i "s/packages=find_packages()/packages=find_packages(exclude=['tests', 'tests.*'])/" setup.py || die
	distutils-r1_python_prepare_all
}

python_test() {
	"${EPYTHON}" -c 'import nose; nose.run_exit()' -vv --detailed-errors --with-coverage --cover-package=preggy --cover-package=preggy.assertions --cover-package=preggy.assertions.types tests/ ||
	die "tests failed with ${EPYTHON}"
}
