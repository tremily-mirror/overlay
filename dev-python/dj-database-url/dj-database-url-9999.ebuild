# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_COMPAT=( python{2_7,3_3,3_4} pypy )

inherit distutils-r1

if [[ "${PV}" == "9999" ]]; then
	inherit git-2
	EGIT_REPO_URI="git://github.com/kennethreitz/${PN}.git"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"
fi

DESCRIPTION="Use Database URLs in your Django Application"
HOMEPAGE="http://github.com/kennethreitz/dj-database-url https://pypi.python.org/pypi/dj-database-url"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND="
	dev-python/django[${PYTHON_USEDEP}]
	"
DEPEND="
	dev-python/setuptools[${PYTHON_USEDEP}]
	"

python_test() {
	"${EPYTHON}" test_dj_database_url.py || die "Tests fail with ${EPYTHON}"
}
