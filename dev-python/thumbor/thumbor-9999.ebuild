# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=6
PYTHON_COMPAT=( python2_7 )

inherit distutils-r1 user

if [[ "${PV}" == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="git://github.com/thumbor/thumbor.git"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"
fi

DESCRIPTION="An an open-source photo thumbnail service"
HOMEPAGE="https://github.com/thumbor/thumbor https://pypi.python.org/pypi/thumbor"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND="
	>=dev-python/derpconf-0.2.0[${PYTHON_USEDEP}]
	dev-python/futures[${PYTHON_USEDEP}]
	>=dev-python/libthumbor-1.3.2[${PYTHON_USEDEP}]
	>=dev-python/pexif-0.15[${PYTHON_USEDEP}]
	<dev-python/pexif-1.0[${PYTHON_USEDEP}]
	>=dev-python/pillow-3.0.0[${PYTHON_USEDEP}]
	<dev-python/pillow-4.0.0[${PYTHON_USEDEP}]
	>=dev-python/pycrypto-2.1.0[${PYTHON_USEDEP}]
	>=dev-python/pycurl-7.19.0[${PYTHON_USEDEP}]
	<dev-python/pycurl-7.44.0[${PYTHON_USEDEP}]
	dev-python/pytz[${PYTHON_USEDEP}]
	>=dev-python/statsd-3.0.1[${PYTHON_USEDEP}]
	media-libs/opencv[python_single_target_python2_7]
	>=www-servers/tornado-4.1.0[${PYTHON_USEDEP}]
	<www-servers/tornado-5.0.0[${PYTHON_USEDEP}]
	"
DEPEND="
	dev-python/setuptools[${PYTHON_USEDEP}]
	test? (
		${RDEPEND}
		dev-python/colorama[${PYTHON_USEDEP}]
		dev-python/coverage[${PYTHON_USEDEP}]
		dev-python/flake8[${PYTHON_USEDEP}]
		>=dev-python/mock-1.0.1[${PYTHON_USEDEP}]
		<dev-python/mock-3.0.0[${PYTHON_USEDEP}]
		dev-python/nose[${PYTHON_USEDEP}]
		dev-python/numpy[${PYTHON_USEDEP}]
		>=dev-python/preggy-1.3.0[${PYTHON_USEDEP}]
		dev-python/pyssim[${PYTHON_USEDEP}]
		dev-python/raven[${PYTHON_USEDEP}]
		>=dev-python/redis-py-2.4.9[${PYTHON_USEDEP}]
		<dev-python/redis-py-3.0.0[${PYTHON_USEDEP}]
		>=dev-python/scipy-0.16.1[${PYTHON_USEDEP}]
		<dev-python/scipy-1.0.0[${PYTHON_USEDEP}]
		media-gfx/cairosvg[${PYTHON_USEDEP}]
	)
	"

PATCHES=( "${FILESDIR}/${PV}-0001-setup.py-Remove-argparse-dependency.patch" )

pkg_setup() {
	P_HOME="${EPREFIX}/var/lib/${PN}"
	ebegin "Creating ${PN} user and group"
	enewgroup "${PN}"
	enewuser "${PN}" -1 -1 "${P_HOME}" "${PN}"
	eend $?
}

python_test() {
	make test || die "Testsuite failed under ${EPYTHON}"
}

python_install_all() {
	distutils-r1_python_install_all
	doinitd "${FILESDIR}/init.d/${PN}"
	mkdir "${ED}etc/${PN}" || die "Failed to make ${EPREFIX}/etc/${PN}"
	PYTHONPATH="${ED}/usr/lib64/python2.7/site-packages" "${ED}/usr/lib/python-exec/python2.7/thumbor-config" \
		> "${ED}etc/${PN}/${PN}.conf" ||
		die "Failed to run thumbor-config"
	mkdir -p "${ED}var/lib/${PN}" || die "Failed to make ${EPREFIX}/var/lib/${PN}"
	chown -R "${PN}:${PN}" "${ED}etc/${PN}" "${ED}var/lib/${PN}" || die "Failed to chown ${PN}:${PN}"
}
