# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
PYTHON_COMPAT=( python{2_7,3_3,3_4} pypy )

inherit distutils-r1

if [[ "${PV}" == "9999" ]]; then
	inherit git-2
	EGIT_REPO_URI="git://github.com/getsentry/${PN}-python.git"
	SRC_URI=""
else
	SRC_URI="mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"
fi

DESCRIPTION="Raven is a client for Sentry"
HOMEPAGE="http://github.com/getsentry/raven-python"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND=""
DEPEND="${RDEPEND}
	test? (
		dev-python/anyjson[${PYTHON_USEDEP}]
		>=dev-python/blinker-1.1[${PYTHON_USEDEP}]
		dev-python/bottle[${PYTHON_USEDEP}]
		>=dev-python/celery-2.5[${PYTHON_USEDEP}]
		>=dev-python/django-1.4[${PYTHON_USEDEP}]
		>=dev-python/django-celery-2.5[${PYTHON_USEDEP}]
		>=dev-python/exam-0.5.2[${PYTHON_USEDEP}]
		>=dev-python/flask-0.8[${PYTHON_USEDEP}]
		>=dev-python/flask-login-0.2.0[${PYTHON_USEDEP}]
		dev-python/logbook[${PYTHON_USEDEP}]
		dev-python/mock[${PYTHON_USEDEP}]
		dev-python/nose[${PYTHON_USEDEP}]
		dev-python/pep8[${PYTHON_USEDEP}]
		dev-python/pytest[${PYTHON_USEDEP}]
		>=dev-python/pytest-cov-1.4[${PYTHON_USEDEP}]
		dev-python/pytest-django[${PYTHON_USEDEP}]
		dev-python/pytz[${PYTHON_USEDEP}]
		dev-python/webob[${PYTHON_USEDEP}]
		dev-python/webtest[${PYTHON_USEDEP}]
		www-servers/tornado[${PYTHON_USEDEP}]
		python_targets_python3_3? (
			dev-python/aiohttp[python_targets_python3_3]
		)
		python_targets_python3_4? (
			dev-python/aiohttp[python_targets_python3_4]
		)
		python_targets_python2_7? (
			dev-python/paste[python_targets_python2_7]
			dev-python/unittest2[python_targets_python2_7]
			dev-python/webpy[python_targets_python2_7]
		)
	)"

python_test() {
	py.test || die "Testsuite failed under ${EPYTHON}"
}
