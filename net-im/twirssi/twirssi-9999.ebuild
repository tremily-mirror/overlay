# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

if [ "${PV}" == "9999" ]; then
	inherit git-2
	EGIT_REPO_URI="git://github.com/zigdon/twirssi.git"
	SRC_URI=""
else
	SRC_URI="http://twirssi.com/${PN}.pl"
fi

DESCRIPTION="Post to Twitter and Identi.ca from Irssi."
HOMEPAGE="http://twirssi.com/"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="doc"

DEPEND="net-irc/irssi[perl]
	dev-perl/DateTime
	dev-perl/DateTime-Format-Strptime
	dev-perl/HTML-Parser
	dev-perl/HTTP-Date
	dev-perl/JSON-Any
	dev-perl/Net-Twitter
	dev-perl/WWW-Shorten
	dev-perl/libwww-perl
	perl-core/Data-Dumper
	perl-core/Encode
	"
RDEPEND="${DEPEND}"

src_unpack() {
	if [ "${PV}" == "9999" ]; then
		git-2_src_unpack
	else
		unpack "${A}"
	fi
	cd "${S}"
}

src_install() {
	insinto "/usr/share/irssi/scripts/"
	doins twirssi.pl
	dodoc README
	if use doc; then
		dohtml -r html/*
	fi
}
