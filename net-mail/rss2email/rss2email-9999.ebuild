# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

PYTHON_DEPEND="3:3.2"
SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="2.* 3.[01]"

inherit eutils distutils

if [[ "${PV}" == "9999" ]] ; then
	inherit git-2
	EGIT_BRANCH="master"
	EGIT_REPO_URI="git://github.com/wking/${PN}.git"
	SRC_URI=""
else
	SRC_URI="https://github.com/wking/${PN}/tarball/v${PV} -> ${P}.tar.gz"
fi

DESCRIPTION="A python script that converts RSS/Atom newsfeeds to email"
HOMEPAGE="https://github.com/wking/rss2email/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-python/feedparser-5.0.1
	>=dev-python/html2text-3.01"

src_install() {
	distutils_src_install
	dodoc CHANGELOG README
	doman r2e.1
}
