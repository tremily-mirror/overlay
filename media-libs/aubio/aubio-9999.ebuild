# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
PYTHON_COMPAT=( python{2_6,2_7} )

inherit distutils-r1 eutils waf-utils

if [[ "${PV}" == "9999" ]]; then
	inherit git-2
	EGIT_BRANCH="develop"
	EGIT_REPO_URI="git://git.aubio.org/git/${PN}"
	SRC_URI=""
else
	SRC_URI="http://aubio.piem.org/pub/${P}.tar.gz"
fi

DESCRIPTION="Library for audio labelling"
HOMEPAGE="http://aubio.piem.org"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc double-precision examples fftw jack lash libsamplerate sndfile"

RDEPEND="${PYTHON_DEPS}
	fftw? ( =sci-libs/fftw-3* )
	jack? ( media-sound/jack )
	lash? ( >=media-sound/lash-0.5.0 )
	libsamplerate? ( media-libs/libsamplerate )
	sndfile? ( >=media-libs/libsndfile-1.0.4 )
	"
DEPEND="${RDEPEND}
	dev-util/pkgconfig
	doc? ( app-doc/doxygen app-text/docbook2X virtual/latex-base )"

# For Python and distutils
BUILD_DIR="${S}/python"

src_prepare() {
	epatch "${FILESDIR}"/aubio-9999-*.patch

	cd "${BUILD_DIR}"
	distutils-r1_src_prepare
}

src_configure() {
	# aubio's waf script isn't compatible with Python 3.  Setup so that
	# the call in waf-utils_src_configure uses a version of Python 2.
	python_export_best

	# waf: error: no such option: --libdir
	NO_WAF_LIBDIR=1

	waf-utils_src_configure \
		--enable-complex \
		$(use_enable double-precision double) \
		$(use_enable fftw fftw3) \
		$(use_enable jack) \
		$(use_enable lash) \
		$(use_enable sndfile) \
		$(use_enable libsamplerate samplerate)

	cd "${BUILD_DIR}"
	distutils-r1_src_configure
}

src_compile() {
	waf-utils_src_compile	

	if use doc; then
		export VARTEXFONTS="${T}/fonts"
		cd "${S}"/doc
		doxygen full.cfg
	fi

	cd "${BUILD_DIR}"
	distutils-r1_src_compile
}

src_install() {
	waf-utils_src_install

	dodoc AUTHORS ChangeLog README.md
	newdoc python/README README.python

	if use doc; then
		dohtml -r doc/full/html/*
	fi

	if use examples; then
		insinto "/usr/share/doc/${PF}/examples"
		doins -r python/demos
		docinto examples
		newdoc python/README README.examples
	fi

	find "${ED}"usr -name '*.la' -exec rm -f {} +

	DOCS=()
	cd "${BUILD_DIR}"
	distutils-r1_src_install
}
