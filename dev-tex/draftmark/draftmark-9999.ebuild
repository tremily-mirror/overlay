# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit latex-package

if [[ ${PV} == "9999" ]] ; then
	SRC_URI="http://mirror.ctan.org/macros/latex/contrib/draftmark.zip"
else
	SRC_URI="?"
fi

DESCRIPTION="LaTeX package to put draft marks on selected pages."
HOMEPAGE="http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=draftmark"

LICENSE="noncommercial"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	dev-texlive/texlive-latex
	dev-texlive/texlive-latexrecommended
	dev-texlive/texlive-latexextra
	dev-tex/xcolor"
DEPEND="${RDEPEND}
	app-arch/unzip"

# dev-texlive/texlive-latex
#   fix-cm
#   graphicx
# dev-texlive/texlive-latexrecommended
#   atbegshi
#   picture
#   xkeyval
# dev-texlive/texlive-latexextra
#   etextools
#   etoolbox
#   lastpage
#   ltxnew
#   pagerange
#   xifthen

S="${WORKDIR}/${PN}"

TEXMF=/usr/share/texmf-site
