# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit latex-package

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_BRANCH="master"
	EGIT_REPO_URI="git://tremily.us/drexel-thesis.git"
	SRC_URI=""
else
	SRC_URI="http://git.tremily.us/?p=${PN}.git;a=snapshot;h=${PV};sf=tgz"
fi

DESCRIPTION="Unofficial Drexel University thesis class."
HOMEPAGE="http://blog.tremily.us/posts/drexel-thesis/"

LICENSE="LPPL-1.3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

RDEPEND="dev-texlive/texlive-latexrecommended
	dev-texlive/texlive-latexextra
	dev-tex/draftmark
	dev-tex/xcolor"
DEPEND="${RDEPEND}"
# For toolchain:
#   app-text/texlive-core
#     bibtex
#   dev-texlive/texlive-latex
#     pdflatex
# For drexel-thesis.cls:
#   dev-texlive/texlive-latex
#     fancyhdr
#     geometry
#     hyperref
#     indentfirst
#     oberdiek (contains hypcap, ifpdf)
#   dev-texlive/texlive-latexrecommended
#     caption
#     eso-pic
#     setspace
#     subfig
#     xkeyval
#   dev-texlive/texlive-latexextra
#     calc
#     floatrow (also contains fr-subfig)
#     graphicx
#     tocloft
#   dev-tex/pgf
#     tikz
# For the documentation:
#   dev-texlive/texlive-latexextra
#     blindtext

src_unpack() {
	if [[ ${PV} == "9999" ]] ; then
		git-2_src_unpack
	else
		unpack ${A}
	fi
	cd "${S}"
}

src_compile() {
	latex-package_src_compile
	emake
}

src_install() {
	latex-package_src_install
	dodoc drexel-logo.pdf
	if use doc ; then
		dodoc drexel-thesis.pdf example.pdf example-draft.pdf
	fi
}
