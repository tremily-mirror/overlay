# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

if [[ ${PV} == "9999" ]] ; then
	inherit bzr
	EBZR_REPO_URI="http://golem.ph.utexas.edu/%7Edistler/code/itexToMML/"
	SRC_URI=""
	S="${WORKDIR}/${P}/itex-src"
else
	SRC_URI="http://golem.ph.utexas.edu/~distler/blog/files/itexToMML-${PV}.tar.gz"
	S="${WORKDIR}/itexToMML/itex-src"
fi

DESCRIPTION="Itex2MML is a */LaTeX into XHTML/MathML converter."
HOMEPAGE="http://golem.ph.utexas.edu/~distler/blog/itex2MML.html"
LICENSE="GPL-2"

SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}"

src_install() {
	dodir /usr/bin
	cp "${S}/itex2MML" "${D}/usr/bin/"
	dodoc "${S}/../README"
}
