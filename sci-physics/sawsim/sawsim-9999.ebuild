# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
PYTHON_DEPEND="2:2.6"
PYTHON_MODNAME="pysawsim"
SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="3.*"

inherit eutils distutils

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_BRANCH="master"
	EGIT_REPO_URI="git://tremily.us/${PN}.git http://http-git.tremily.us/${PN}.git"
	SRC_URI=""
else
	SRC_URI="http://git.tremily.us/?p=${PN}.git;a=snapshot;h=v${PV};sf=tgz"
fi

DESCRIPTION="sawsim force spectroscopy simulator"
HOMEPAGE="http://blog.tremily.us/posts/${PN}/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="mpi pbs doc"

RDEPEND="dev-lang/python
	dev-python/nose
	dev-python/matplotlib
	dev-python/numpy
	sci-libs/scipy
	mpi? ( dev-python/mpi4py )
	pbs? ( sys-cluster/pbs-python )"
DEPEND="${RDEPEND}
	dev-vcs/git
	app-text/noweb
	sci-libs/gsl
	dev-libs/check
	doc? ( dev-tex/pgf )"

src_unpack() {
	if [[ ${PV} == "9999" ]] ; then
		git-2_src_unpack
	else
		unpack ${A}
	fi
	cd "${S}"
}

src_prepare() {
	notangle -Rmakefile src/sawsim.nw | sed 's/        /\t/' > Makefile
	assert "Failed to generate Makefile"
	distutils_src_prepare
}

src_compile() {
	emake all_bin || die "couldn't build binary targets"
	if use doc ; then
		emake all_doc || die "couldn't build documentation"
	fi
	distutils_src_compile
}

src_install() {
	exeinto /usr/bin/
	doexe bin/sawsim bin/k_model_utils bin/tension_model_utils || die "install failed"
	dodoc README || die "dodoc failed"
	if use doc ; then
		dodoc doc/sawsim.pdf || die "couldn't install manual"
	fi
	distutils_src_install
}
