# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
PYTHON_DEPEND="2:2.7"
SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="3.*"

inherit eutils distutils scons-utils

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_BRANCH="wtk"
	EGIT_REPO_URI="git://tremily.us/${PN}.git"
	SRC_URI=""
else
	SRC_URI="http://code.google.com/p/hooke/downloads/detail?name=${P}.tar.gz"
fi

DESCRIPTION="Force spectroscopy data analysis."
HOMEPAGE="http://code.google.com/p/hooke/"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="doc test X"

RDEPEND="dev-python/matplotlib
	dev-python/numpy
	sci-libs/scipy
	sci-misc/igor
	dev-python/pyyaml
	X? ( >=dev-python/wxpython-2.8 )"
DEPEND="${RDEPEND}
	dev-util/update-copyright
	doc? (
		dev-libs/libxslt
		dev-python/numpydoc
		dev-util/scons
	)
	test? ( dev-python/nose )"
# dev-python/numpydoc is in the science overlay
#   http://overlays.gentoo.org/proj/science/wiki/en

src_unpack() {
	if [[ ${PV} == "9999" ]] ; then
		git-2_src_unpack
	else
		unpack ${A}
	fi
	cd "${S}"
}

src_prepare() {
	if [[ ${PV} == "9999" ]] ; then
		# generates hooke.version
		update-copyright.py
	fi
	distutils_src_prepare
}

src_compile() {
	if use doc ; then
		escons -C doc || die "couldn't build documentation"
	fi
	distutils_src_compile
}

src_install() {
	distutils_src_install
	if use doc ; then
		dohtml -r doc/build/html/*
	fi
	dodoc AUTHORS CHANGELOG README
	docompress -x "/usr/share/doc/${PF}/img"
	insinto "/usr/share/doc/${PF}/img"
	doins doc/img/{hooke.jpg,microscope.ico}
	insinto "/etc/${PN}"
	newins "${FILESDIR}/${P}.cfg" "${PN}.cfg"
}
