# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="6"
PYTHON_COMPAT=( python{3_2,3_3,3_4,3_5,3_6} )

inherit eutils distutils-r1

if [[ "${PV}" == "9999" ]] ; then
	inherit git-r3
	EGIT_REPO_URI="git://tremily.us/${PN}.git"
	SRC_URI=""
else
	SRC_URI="http://git.tremily.us/?p=${PN}.git;a=snapshot;h=v${PV};sf=tgz -> ${P}.tar.gz"
fi

DESCRIPTION="Automatically update copyright blurbs in versioned source."
HOMEPAGE="http://blog.tremily.us/posts/update-copyright/"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
