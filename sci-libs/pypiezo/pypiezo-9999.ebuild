# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
PYTHON_DEPEND="2:2.6"
SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="3.*"

inherit eutils distutils

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_REPO_URI="git://tremily.us/${PN}.git"
	SRC_URI=""
else
	SRC_URI="http://git.tremily.us/?p=${PN}.git;a=snapshot;h=v${PV};sf=tgz"
fi

DESCRIPTION="Piezoelectric positioner control through Python."
HOMEPAGE="http://blog.tremily.us/posts/${PN}/"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="test"

RDEPEND="sci-libs/pycomedi
	dev-python/curses-check-for-keypress
	dev-python/h5config
	dev-python/matplotlib
	dev-python/numpy
	sci-libs/scipy"
DEPEND="$RDEPEND
	test? ( dev-python/nose )"

src_unpack() {
	if [[ ${PV} == "9999" ]] ; then
		git-2_src_unpack
	else
		unpack ${A}
	fi
	cd "${S}"
}

src_install() {
	distutils_src_install
	dodoc README
}
