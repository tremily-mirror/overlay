# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
PYTHON_DEPEND="2:2.6 3:3.2"
SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="3.[01]"

inherit eutils distutils

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_REPO_URI="git://tremily.us/${PN}.git"
	SRC_URI=""
else
	SRC_URI="http://git.tremily.us/?p=${PN}.git;a=snapshot;h=v${PV};sf=tgz"
fi

DESCRIPTION="Pythonic wrappers around the Comedilib device drivers."
HOMEPAGE="http://blog.tremily.us/posts/${PN}/"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="doc +kmod test"

RDEPEND="sci-libs/comedilib[python]
	dev-python/numpy
	sci-libs/scipy
	kmod? ( dev-python/python-kmod )"
DEPEND="$RDEPEND
	>=dev-python/cython-0.14
	test? ( dev-python/nose )"

src_install() {
	distutils_src_install
	dodoc README
	if use doc; then
		dodoc -r doc/
	fi
}
