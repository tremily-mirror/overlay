# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

PYTHON_DEPEND="python? 2:2.7 3"
SUPPORT_PYTHON_ABIS="1"

inherit distutils eutils

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_BRANCH="master"
	EGIT_REPO_URI="git://comedi.org/git/comedi/${PN}.git"
	SRC_URI=""
else
	SRC_URI="http://www.comedi.org/download/${P}.tar.gz"
fi

DESCRIPTION="Userspace interface to Comedi kernel modules."
HOMEPAGE="http://www.comedi.org"

IUSE="python ruby doc"
KEYWORDS="~x86 ~amd64"
LICENSE="LGPL-2.1"
SLOT="0"

PYTHON_MODNAME="comedi.py"
DISTUTILS_SETUP_FILES="${S}/swig/python/setup.py"

DEPEND=">=sci-misc/comedi-headers-${PV}
	sys-devel/flex
	sys-devel/bison
	python? (
		dev-lang/swig
		dev-lang/python
	)
	ruby? (
		dev-lang/swig
		dev-lang/ruby
		app-admin/chrpath
	)
	doc? ( app-text/xmlto )"
RDEPEND=""

src_prepare()
{
	if [ "${PV}" == "9999" ] ; then
		./autogen.sh
	fi
	if use python ; then
		pushd "${S}/swig/python" || die "failed to pushd"
		distutils_src_prepare
		popd || die "failed to popd"
	fi
}

src_configure()
{
	# handle binding compilation and installation ourselves
	#$(use_enable python python-binding)
	#$(use_enable ruby ruby-binding)
	econf \
		--sysconfdir="${EPREFIX}/usr/share/doc/${PF}/etc" \
		--localstatedir="${EPREFIX}/var" \
		--docdir="${EPREFIX}/usr/share/doc/${PF}" \
		--disable-python-binding \
		--disable-ruby-binding \
		$(use_enable doc docbook) \
		|| die "econf failed"
}

src_compile()
{
	emake || die "emake failed"
	if use python ; then
		pushd "${S}/swig/python" || die "failed to pushd"
		distutils_src_compile
		popd || die "failed to popd"
	fi
	if use ruby ; then
		pushd "${S}/swig/ruby" || die "failed to pushd"
		swig -ruby -o ext/comedi_ruby_wrap.c "-I${S}/include" ../comedi.i \
			|| die "swigging ruby binding failed"
		RUBYOPT="" DESTDIR="${D}" ruby setup.rb config --prefix="${D}/usrBBB" \
			-- \
			--with-comedilib-include="${S}/include" \
			--with-comedilib-lib="${S}/lib/.libs" \
			|| die "ruby setup.rb config failed"
		ruby setup.rb setup || die "ruby setup.rb setup failed"
		chrpath -d ext/comedi.so || die "chrpath -d failed"
		popd || die "failed to popd"
	fi
}

src_install()
{
	emake DESTDIR="${D}" install || die "emake install failed"
	keepdir /var/calibrations
	local DOCINTO="/usr/share/doc/${PF}"
	local DDOCINTO="${D}${DOCINTO}"
	if use doc ; then
		epatch "${FILESDIR}/${P}-demo-Makefile.patch"
		pushd "${DDOCINTO}" || die "failed to pushd"
		mv *.conf etc/ || die 'moving *.conf failed'
		popd || die "failed to popd"
		insinto "${DOCINTO}/demo"
		doins demo/Makefile demo/README demo/*.c demo/*.h || die "doins failed"
		if [ -d demo/.deps ]; then
			insinto "${DOCINTO}/demo/.deps"
			doins -r demo/.deps
		fi
		if use python ; then
			insinto "${DOCINTO}/demo/python"
			doins demo/python/README demo/python/*.py || die "doins failed"
		fi
		if use ruby ; then
			insinto "${DOCINTO}/demo/ruby"
			doins swig/ruby/README swig/ruby/demo/*.rb swig/ruby/demo/{cmd,inp,outp} || die "doins failed"
		fi
		docompress -x "${DOCINTO}/demo"
	else
		rm -rf "${DDOCINTO}"
	fi
	if use python ; then
		pushd "${S}/swig/python" || die "failed to pushd"
		distutils_src_install
		popd || die "failed to popd"
	fi
	if use ruby ; then
		pushd "${S}/swig/ruby" || die "failed to pushd"
		ruby setup.rb install || die "ruby setup.rb install failed"
		popd || die "failed to popd"
	fi
	# comedi.h already installed by sci-misc/comedi-headers
	rm "${D}/usr/include/comedi.h" || die "failed to remove comedi.h"
}

pkg_postinst()
{
	if use python ; then
		distutils_pkg_postinst
	fi
}

pkg_postrm()
{
	distutils_pkg_postrm
}
